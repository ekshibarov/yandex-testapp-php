<?php

namespace Application\Model;

/**
 * Class Cost.
 */
class Cost
{
    public function __construct($s_from, $s_to, $price)
    {
        $this->S_from = $s_from;
        $this->S_to = $s_to;
        $this->Cost_Of_1_M2_RUR = $price;
    }

    /**
     * @var int
     */
    private $S_from;

    /**
     * @var int
     */
    private $S_to;

    /**
     * @var int
     */
    private $Cost_Of_1_M2_RUR;

    /**
     * @var int
     */
    private $regionId;

    /**
     * @return int
     */
    public function getSFrom()
    {
        return $this->S_from;
    }

    /**
     * @param int $S_from
     */
    public function setSFrom($S_from)
    {
        $this->S_from = $S_from;
    }

    /**
     * @return int
     */
    public function getSTo()
    {
        return $this->S_to;
    }

    /**
     * @param int $S_to
     */
    public function setSTo($S_to)
    {
        $this->S_to = $S_to;
    }

    /**
     * @return int
     */
    public function getCostOf1M2RUR()
    {
        return $this->Cost_Of_1_M2_RUR;
    }

    /**
     * @param int $Cost_Of_1_M2_RUR
     */
    public function setCostOf1M2RUR($Cost_Of_1_M2_RUR)
    {
        $this->Cost_Of_1_M2_RUR = $Cost_Of_1_M2_RUR;
    }

    /**
     * @return int
     */
    public function getRegionId()
    {
        return $this->regionId;
    }

    /**
     * @param int $regionId
     */
    public function setRegionId($regionId)
    {
        $this->regionId = $regionId;
    }
}
