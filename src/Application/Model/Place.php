<?php

namespace Application\Model;

/**
 * Class Place.
 */
class Place
{
    public function __construct($id, $name, $type)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->costs = new \SplStack();
    }

    /**
     * @param Cost $cost
     */
    public function addCost(Cost $cost)
    {
        $this->costs->push($cost);
    }

    /**
     * @var int
     */
    private $id;

    /**
     * @var Place
     */
    private $parent = null;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \SplStack
     */
    private $costs;

    /**
     * @return \SplStack
     */
    public function getCosts()
    {
        return $this->costs;
    }

    /**
     * @param \SplStack $costs
     */
    public function setCosts($costs)
    {
        $this->costs = $costs;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Place
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Place $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
}
