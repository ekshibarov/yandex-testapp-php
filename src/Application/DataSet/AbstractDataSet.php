<?php

namespace Application\DataSet;


/**
 * Class AbstractDataSet
 * @package Application\DataSet
 */
abstract class AbstractDataSet
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    abstract function load();
}