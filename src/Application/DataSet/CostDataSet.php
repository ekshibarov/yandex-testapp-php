<?php

namespace Application\DataSet;

use Application\Model\Cost;
use Application\Reader\CsvReader;

/**
 * Class CostDataSet
 * @package Application\DataSet
 */
class CostDataSet extends AbstractDataSet
{

    /**
     * @var
     */
    private $filename;

    /**
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
        $this->load();
    }

    /**
     * @return array
     */
    public function load() {
        $costReader = new CsvReader($this->filename);

        foreach ($costReader->read() as $row) {

            /**
             * Create new cost
             */
            $cost = new Cost($row[0], $row[1], $row[2]);
            $this->data[$row[3]][] = $cost;
        }
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }
}
