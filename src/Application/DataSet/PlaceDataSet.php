<?php

namespace Application\DataSet;


use Application\Model\Cost;
use Application\Model\Place;
use Application\Reader\CsvReader;

/**
 * Class PlaceDataSet
 * @package Application\DataSet
 */
class PlaceDataSet extends AbstractDataSet
{

    /**
     * @var
     */
    private $filename;

    /**
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
        $this->load();
    }

    /**
     * @return array
     */
    public function load()
    {
        $cityReader = new CsvReader($this->filename);

        foreach ($cityReader->read() as $row) {

            /**
             * Create new place
             * @var Place $place
             */
            $place = new Place($row[2], $row[0], $row[1]);
            $parent_id = $row[3];
            if (array_key_exists($parent_id, $this->data)) {
                $place->setParent($this->data[$parent_id]);
            }
            $this->data[$place->getId()]= $place;
        }
        return $this->data;
    }

    /**
     * @param $region_id
     * @param $s
     * @return null|integer
     */
    public function getPrice($region_id, $s)
    {
        /**
         * @var Place $place
         */
        $place = $this->data[$region_id];
        if ($place) {
            $child = $place;
            do {
                /**
                 * @var Cost $cost
                 */
                foreach ($child->getCosts() as $cost) {
                    if (($s >= $cost->getSFrom()) && ($s <= $cost->getSTo())) {
                        $price = $s * $cost->getCostOf1M2RUR();
                        return $price;
                    }
                }
            } while (($child = $child->getParent()));
        }
        return null;
    }

    /**
     * @param CostDataSet $costDataSet
     * @return array
     */
    public function addCostsToPlaces(CostDataSet $costDataSet) {
        foreach($costDataSet->getData() as $region_id => $costs) {
            foreach ($costs as $cost) {
                /**
                 * @var Place $place
                 */
                $place = $this->data[$region_id];
                $place->addCost($cost);
            }
        }
        return $this->data;
    }


    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }
}