<?php

namespace Application\Reader;

class CsvReader extends AbstractReader
{
    public function read()
    {
        $file = fopen($this->filename, 'r');
        while (($data = fgetcsv($file, 0, ';'))) {
            yield $data;
        }
    }
}
