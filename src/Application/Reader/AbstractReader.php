<?php

namespace Application\Reader;

/**
 * Class AbstractReader.
 */
abstract class AbstractReader
{
    /**
     * @var string
     */
    protected $filename;

    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    abstract public function read();
}
