<?php

require_once __DIR__ . '/vendor/autoload.php';

use Application\DataSet\CostDataSet;
use Application\DataSet\PlaceDataSet;

$costDataSet = new CostDataSet("data/cost.csv");
$placeDataSet = new PlaceDataSet("data/city.csv");

$placeDataSet->addCostsToPlaces($costDataSet);

$in = fopen('php://stdin','r');

while(!feof($in)) {
    $line = fgets($in);
    if (preg_match('/\d+,\d+/', $line)) {

        // Parse input string
        $params = mb_split(",", $line);
        $region_id = rtrim($params[0]);
        $s = rtrim($params[1]);

        // Get price and show
        $price = $placeDataSet->getPrice($region_id, $s);
        if ($price) {
            echo "{$price}\n";
        }
        else {
            echo "false\n";
        }
    }
    else {
        echo "Invalid parameter. It should be in    'region_id,s' format\n";
    }
}

